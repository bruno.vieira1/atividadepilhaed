package desafioPontuadoED;

import java.util.Scanner;
import java.util.Stack;

public class Program {
	static Scanner sc = new Scanner(System.in);
	static Stack<Integer> pilha = new Stack<Integer>();

	public static void main(String[] args) {

		Integer natural;

		do {
			System.out.println("Informar valor natural a ser armazenado: ");
			natural = sc.nextInt();
			if (natural >= 0) {
				pilha.push(natural);

			}
		}

		while (natural >= 0);

		System.out.println("Valores armazenados: ");
		for (Integer identificador : pilha) {
			System.out.println(identificador);
		}
		System.out.println("Quantidade de ocorrencias de primos seguidos: " + Contador(pilha));
	}

	public static Integer Contador(Stack<Integer> c) {
		Integer cont = 0;
		for (int i = 0; i < c.size() - 1; i++) {
			if (c.elementAt(i) % 2 != 0 && c.elementAt(i + 1) % 2 != 0) {
				cont++;
			}

		}
		return cont;

	}
}
